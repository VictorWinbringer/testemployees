﻿using System.Collections.Generic;
using System.Linq;
using SalaryCalculation.Entities.Abstract;

namespace SalaryCalculation.DomainServices
{
    public sealed class SalaryCalculator : ISalaryCalculator
    {
        public decimal Salary(EmployeeBase employee) => employee.TotalSalary();

        public decimal Salary(IEnumerable<EmployeeBase> employees) => employees.Sum(x => x.TotalSalary());
    }
}
