﻿using System.Collections.Generic;
using SalaryCalculation.Entities.Abstract;

namespace SalaryCalculation.DomainServices
{
    public interface ISalaryCalculator
    {
        decimal Salary(EmployeeBase employee);
        decimal Salary(IEnumerable<EmployeeBase> employees);
    }
}