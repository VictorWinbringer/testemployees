﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SalaryCalculation.Entities
{
    public sealed class SalaryCalculationException : ApplicationException
    {
        public SalaryCalculationException()
        {
        }

        public SalaryCalculationException(string message) : base(message)
        {
        }

        public SalaryCalculationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SalaryCalculationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
