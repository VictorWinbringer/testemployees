﻿using System;
using SalaryCalculation.Entities.Abstract;

namespace SalaryCalculation.Entities
{
    public sealed class Employee : EmployeeBase
    {
        public Employee(string name, DateTime dateJoined) : base(name, dateJoined, 0.3m, 0.03m)
        {
        }
    }
}