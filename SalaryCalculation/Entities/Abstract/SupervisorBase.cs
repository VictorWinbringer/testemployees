﻿using System;
using System.Collections.Generic;

namespace SalaryCalculation.Entities.Abstract
{
    public abstract class SupervisorBase : EmployeeBase
    {
        protected readonly decimal Share;
        private readonly List<EmployeeBase> _subordinates;

        protected SupervisorBase(string name, DateTime dateJoined, decimal maxAllowance, decimal yearAllowance, decimal share) : base(name, dateJoined, maxAllowance, yearAllowance)
        {
            _subordinates = new List<EmployeeBase>();
            Share = share;
            Validate();
        }

        public IReadOnlyList<EmployeeBase> Subordinates => _subordinates.AsReadOnly();

        public void Add(EmployeeBase subordinate)
        {
            if (subordinate == null)
                throw new SalaryCalculationException("Subordinate is null");
            if (_subordinates.Contains(subordinate))
                throw new SalaryCalculationException("Already have this subordinate");
            _subordinates.Add(subordinate);
            if (subordinate is SupervisorBase sb)
                CheckCircle(sb);
            if (!object.ReferenceEquals(subordinate.Supervisor, this))
                subordinate.Supervisor = this;
        }

        private void CheckCircle(SupervisorBase sb)
        {
            foreach (var e in sb._subordinates)
            {
                if (e == this)
                    throw new SalaryCalculationException("Have reference circle");
                if (e is SupervisorBase s)
                    CheckCircle(s);
            }
        }

        public void Remove(EmployeeBase subordinate)
        {
            if (subordinate == null)
                throw new SalaryCalculationException("Subordinate is null");
            _subordinates.Remove(subordinate);
            if (object.ReferenceEquals(subordinate.Supervisor, this))
                subordinate.Supervisor = null;
        }

        public void AddRange(IEnumerable<EmployeeBase> employees)
        {
            foreach (var e in employees)
            {
                Add(e);
            }
        }

        public void RemoveRange(IEnumerable<EmployeeBase> employees)
        {
            foreach (var e in employees)
            {
                Remove(e);
            }
        }

        private void Validate()
        {
            if (_subordinates == null)
                throw new SalaryCalculationException("Subordinates are null");
            if (Share < 0)
                throw new SalaryCalculationException("Share < 0");
        }
    }
}