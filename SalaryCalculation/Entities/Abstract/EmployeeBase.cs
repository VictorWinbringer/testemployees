﻿using System;
using System.Linq;

namespace SalaryCalculation.Entities.Abstract
{

    public abstract class EmployeeBase : IEquatable<EmployeeBase>
    {
        private readonly decimal _maxAllowance;
        private readonly decimal _yearAllowance;
        private SupervisorBase _supervisor;
        protected const decimal BASE_SALARY = 1;

        protected EmployeeBase(string name, DateTime dateJoined, decimal maxAllowance, decimal yearAllowance)
        {
            _maxAllowance = maxAllowance;
            _yearAllowance = yearAllowance;
            Name = name;
            DateJoined = dateJoined;
            Validate();
        }

        public string Name { get; }
        public DateTime DateJoined { get; }

        public SupervisorBase Supervisor
        {
            get => _supervisor;
            set
            {
                var old = _supervisor;
                _supervisor = value;
                if (old != null && old.Subordinates.Contains(this))
                    old.Remove(this);
                if (value != null && !value.Subordinates.Contains(this))
                    value.Add(this);
            }
        }

        public virtual decimal TotalSalary()
        {
            var years = DateTime.UtcNow.Year - DateJoined.Date.Year;
            var totalAllowance = _yearAllowance * years;
            totalAllowance = totalAllowance > _maxAllowance ? _maxAllowance : totalAllowance;
            return BASE_SALARY + (BASE_SALARY * totalAllowance);
        }

        private void Validate()
        {
            if (string.IsNullOrWhiteSpace(Name))
                throw new SalaryCalculationException("Employee name is null or white space!");
            if (DateJoined == default)
                throw new SalaryCalculationException("Date joined is default");
            if (DateJoined > DateTime.Now)
                throw new SalaryCalculationException("Date joined is from feature!");
            if (_maxAllowance < 0)
                throw new SalaryCalculationException("Max allowance < 0");
            if (_yearAllowance < 0)
                throw new SalaryCalculationException("Year Allowance < 0");
        }

        public bool Equals(EmployeeBase other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (obj is EmployeeBase e)
                return Equals(e);
            return false;
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }
    }
}
