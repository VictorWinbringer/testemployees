﻿using System;
using System.Linq;
using SalaryCalculation.Entities.Abstract;

namespace SalaryCalculation.Entities
{
    public sealed class Manager : SupervisorBase
    {
        public Manager(string name, DateTime dateJoined) : base(name, dateJoined, 0.4m, 0.05m, 0.005m)
        {
        }

        public override decimal TotalSalary()
        {
            return base.TotalSalary() + Subordinates.Sum(x => x.TotalSalary() * Share);
        }
    }
}