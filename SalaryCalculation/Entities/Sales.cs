﻿using System;
using System.Collections.Generic;
using System.Linq;
using SalaryCalculation.Entities.Abstract;

namespace SalaryCalculation.Entities
{
    public sealed class Sales : SupervisorBase
    {
        public Sales(string name, DateTime dateJoined) : base(name, dateJoined, 0.35m, 0.01m, 0.03m)
        {
        }

        public override decimal TotalSalary()
        {
            var employees = new List<EmployeeBase>();
            employees.Add(this);
            return base.TotalSalary() + Subordinates.Sum(x => SalaryShare(x, employees));
        }

        private decimal SalaryShare(EmployeeBase employee, List<EmployeeBase> employees)
        {
            if (employees.Contains(employee))
                return 0;
            employees.Add(employee);
            employees.Add(employee);
            return (employee.TotalSalary() * Share) +
                   (employee is SupervisorBase sb ? sb.Subordinates.Sum(x => SalaryShare(x, employees)) : 0m);
        }
    }
}