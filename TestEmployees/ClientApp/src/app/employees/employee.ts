export class Employee {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public middleName?: string,
    public phoneNumber?: string,
    public email?: string,
    public employmentDate?: Date,
    public concurrencyStamp?: string) { }
}
