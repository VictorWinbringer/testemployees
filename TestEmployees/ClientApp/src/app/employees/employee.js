"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Employee = /** @class */ (function () {
    function Employee(id, firstName, lastName, middleName, phoneNumber, email, employmentDate, concurrencyStamp) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.employmentDate = employmentDate;
        this.concurrencyStamp = concurrencyStamp;
    }
    return Employee;
}());
exports.Employee = Employee;
//# sourceMappingURL=employee.js.map