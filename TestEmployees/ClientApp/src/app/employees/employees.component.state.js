"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EmployeeComponentState;
(function (EmployeeComponentState) {
    EmployeeComponentState[EmployeeComponentState["Table"] = 0] = "Table";
    EmployeeComponentState[EmployeeComponentState["Edit"] = 1] = "Edit";
    EmployeeComponentState[EmployeeComponentState["Create"] = 2] = "Create";
})(EmployeeComponentState = exports.EmployeeComponentState || (exports.EmployeeComponentState = {}));
//# sourceMappingURL=EmployeeComponentState.js.map