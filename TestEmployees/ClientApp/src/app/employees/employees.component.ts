import { Component, OnInit } from '@angular/core';
import { EmployeesService } from './employees.service';
import { Employee } from './employee';
import { Observable } from 'rxjs';
import { EmployeeComponentState } from "./employees.component.state";

@Component({
  selector: 'app-employees',
  styles: [`
        input.ng-touched.ng-invalid {border:solid red 2px;}
        input.ng-touched.ng-valid {border:solid green 2px;}
    `],
  templateUrl: './employees.component.html',
  providers: [EmployeesService]
})
export class EmployeesComponent implements OnInit {

  employee: Employee = new Employee();   // выбранный товар
  employees: Employee[];                // массив товаров
  state: EmployeeComponentState = EmployeeComponentState.Table;          // табличный режим
  errors: string[] = [];

  constructor(private dataService: EmployeesService) { }
  get isTable() {
    return this.state === EmployeeComponentState.Table;
  }
  ngOnInit() {
    this.loadProducts();    // загрузка данных при старте компонента  
  }
  // получаем данные через сервис
  loadProducts() {
    this.errors = [];
    this.dataService.getAll()
      .subscribe((data: Employee[]) => this.employees = data, this.handle.bind(this));
  }
  // сохранение данных
  save() {
    if (this.state === EmployeeComponentState.Create) {
      this.dataService.create(this.employee).subscribe(data => this.loadProducts(), this.handle.bind(this));
    } else if (this.state === EmployeeComponentState.Edit) {
      this.dataService.update(this.employee).subscribe(data => this.loadProducts(), this.handle.bind(this));
    }
    this.cancel();
  }

  edit(p: Employee) {
    this.employee = p;
    this.state = EmployeeComponentState.Edit;
  }

  cancel() {
    this.employee = new Employee();
    this.state = EmployeeComponentState.Table;
  }

  delete(p: Employee) {
    let ok = confirm("Вы уверены что хотите удалить сотрудника из БД?");
    if (ok) {
      this.dataService.delete(p.id).subscribe(data => this.loadProducts(), this.handle.bind(this));
    }
  }

  handle(error: any) {
    switch (error.status) {
      case 401:
        this.errors.push('Вы не авторизованы!');
        return;
      case 403:
        this.errors.push('У вас не достаточно прав для совершения этого действия! Зайдите по аккаунтом администратора!');
        return;
      default:
        this.errors.push(JSON.stringify(error));
    }
  }

  add() {
    this.cancel();
    this.state = EmployeeComponentState.Create;
  }
}
