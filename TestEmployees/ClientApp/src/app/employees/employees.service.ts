import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from './employee';

@Injectable()
export class EmployeesService {

  private url = "/employees";

  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get(this.url);
  }

  create(product: Employee) {
    return this.http.post(this.url, product);
  }

  update(product: Employee) {
    return this.http.put(this.url, product);
  }

  delete(id: number) {
    return this.http.delete(this.url + '/' + id);
  }
}
