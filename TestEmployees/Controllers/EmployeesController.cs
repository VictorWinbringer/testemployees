﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using TestEmployees.Data;
using TestEmployees.Domain;

namespace TestEmployees.Controllers
{
    //Так как это тестовое задание то смысла мудрить с абстракциями нет.
    [ApiController]
    [Route("[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public EmployeesController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Authorize] //Так как читать может любой пользователь с ролью "пользователь" и по идее она есть у всех то и специально ее указывать нет смысла
        [HttpGet]
        public async Task<List<Employee>> Get()
        {
            return await _context.Employees.ToListAsync();
        }

        [Authorize(Policy = Constants.ADMIN_POLICY)]
        [HttpPost]
        public async Task<int> Create(Employee employee)
        {
            employee.Id = 0;
            _context.Employees.Add(employee);
            await _context.SaveChangesAsync();
            return employee.Id;
        }

        [Authorize(Policy = Constants.ADMIN_POLICY)]
        [HttpPut]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(400, Type = typeof(ModelStateDictionary))]
        public async Task<ActionResult> Update(Employee employee)
        {
            if (employee.Id < 1)
            {
                ModelState.AddModelError("", "Идентификатор сотрудника меньше 1");
                return BadRequest(ModelState);
            }
            _context.Employees.Update(employee);
            return Ok(await _context.SaveChangesAsync());
        }

        [Authorize(Policy = Constants.ADMIN_POLICY)]
        [HttpDelete("{id}")]
        public async Task<int> Delete(int id)
        {
            var e = await _context.Employees.FindAsync(id);
            if (e == null)
                return 0;
            _context.Employees.Remove(e);
            return await _context.SaveChangesAsync();
        }
    }
}
