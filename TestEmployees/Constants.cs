namespace TestEmployees
{
    public static class Constants
    {
        public const string ADMIN_EMAIL = "admin@mail.ru";
        public const string ADMIN_ROLE = "admin";
        public const string ADMIN_POLICY = "admins";
    }
}