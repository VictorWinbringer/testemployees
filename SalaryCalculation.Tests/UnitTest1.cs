using System;
using System.Collections.Generic;
using System.Linq;
using DddBase;
using SalaryCalculation.DomainServices;
using SalaryCalculation.Entities;
using SalaryCalculation.Entities.Abstract;
using Xunit;

namespace SalaryCalculation.Tests
{
    public class UnitTest1
    {

        public sealed class TestEntity : EntityBase<string>
        {
            public TestEntity(string id)
            {
                Id = id;
            }

            public override string Id { get; }
        }

        public sealed class ValueObjTestStr : ValueObjectBase
        {
            public string Value { get; }

            public ValueObjTestStr(string value)
            {
                Value = value;
            }

            protected override IEnumerable<object> GetAtomicValues()
            {
                yield return Value;
            }
        }

        public sealed class ValueObjTestInt : ValueObjectBase
        {
            public int Value { get; }

            public ValueObjTestInt(int value)
            {
                Value = value;
            }

            protected override IEnumerable<object> GetAtomicValues()
            {
                yield return Value;
            }
        }

        [Fact]
        public void Test1()
        {
            var str = new ValueObjTestStr("1");
            var i = new ValueObjTestInt(1);
            var ent = new TestEntity("1");
            var eq = i == str;
            var dd = new List<EmployeeBase>()
            {
                new Employee("1", DateTime.Now),
                new Employee("4", DateTime.Now),
                new Employee("4", DateTime.Now),
                new Manager("1", DateTime.Now),
                new Manager("3", DateTime.Now),
                new Manager("3", DateTime.Now),
                new Sales("1", DateTime.Now),
                new Sales("2", DateTime.Now),
                new Sales("2", DateTime.Now),
            }.Distinct().ToList();


            ISalaryCalculator s = new SalaryCalculator();
            var e = new Employee("sd", DateTime.Now.AddYears(-3));
            var e2 = new Employee("sdddd", DateTime.Now.AddYears(-3));
            var manager = new Manager("sdf", DateTime.Now.AddYears(-3));
            manager.Add(e);
            var sales = new Sales("sadfsaf", DateTime.Now.AddYears(-3));
            sales.Add(e2);
            sales.Add(manager);
            var sales2 = new Sales("sadfsaf", DateTime.Now.AddYears(-40));
            sales2.Add(sales);
            sales2.AddRange(dd);
            var es = new List<EmployeeBase>
            {
                e,
                manager,
                sales,
                e2,
                sales2
            };

            var salary = s.Salary(sales2);
            var salary2 = s.Salary(es);
        }
    }
}
