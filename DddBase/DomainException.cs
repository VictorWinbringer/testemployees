﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DddBase
{

    [Serializable]
    public sealed class DomainException : Exception
    {
        public DomainException(string message, Dictionary<string, object> data = null) : base(message)
        {
            if (data != null)
            {
                foreach (var kv in data)
                {
                    Data[kv.Key] = kv.Value;
                }
            }
        }

        private DomainException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
